//-------------------------------------------------------------------------
//    Ball.sv                                                            --
//    Viral Mehta                                                        --
//    Spring 2005                                                        --
//                                                                       --
//    Modified by Stephen Kempf 03-01-2006                               --
//                              03-12-2007                               --
//    Translated by Joe Meng    07-07-2013                               --
//    Modified by Po-Han Huang  12-08-2017                               --
//    Spring 2018 Distribution                                           --
//                                                                       --
//    For use with ECE 385 Lab 8                                         --
//    UIUC ECE Department                                                --
//-------------------------------------------------------------------------


module  ball ( input         Clk,                // 50 MHz clock
                             Reset,              // Active-high reset signal
                             frame_clk,          // The clock indicating a new frame (~60Hz)
									  move_left,
									  move_right,
									  stand_still,
					input [7:0]   keycode,
					input [31:0]  ADCDATA,
               input [9:0]   DrawX, DrawY,       // Current pixel coordinates
					input logic   is_dead,
               output logic  is_ball,             // Whether current pixel belongs to ball or background
					output logic  is_wall,					//whether current pixel belongs to wall or background
					output logic [18:0] score,
					output logic  won
              );
    
    parameter [9:0] Ball_X_Center = 10'd320;  // Center position on the X axis
    parameter [9:0] Ball_Y_Center = 10'd240;  // Center position on the Y axis
    parameter [9:0] Ball_X_Min = 10'd0;       // Leftmost point on the X axis
    parameter [9:0] Ball_X_Max = 10'd639;     // Rightmost point on the X axis
    parameter [9:0] Ball_Y_Min = 10'd0;       // Topmost point on the Y axis
    parameter [9:0] Ball_Y_Max = 10'd479;     // Bottommost point on the Y axis
    parameter [9:0] Ball_X_Step = 10'd1;      // Step size on the X axis
    parameter [9:0] Ball_Y_Step = 10'd1;      // Step size on the Y axis
    parameter [9:0] Ball_Size = 10'd4;        // Ball size
    
    logic [9:0] Ball_X_Pos, Ball_X_Motion, Ball_Y_Pos, Ball_Y_Motion;
    logic [9:0] Ball_X_Pos_in, Ball_X_Motion_in, Ball_Y_Pos_in, Ball_Y_Motion_in;

	 
	 logic [18:0] nextScore;
	 
	 assign won = score[18];
	 
	 logic [15:0] leftChannelSamples [48];
	 logic [15:0] rightChannelSamples [48];
    logic [7:0] counter;
    //////// Do not modify the always_ff blocks. ////////
    // Detect rising edge of frame_clk
    logic frame_clk_delayed, frame_clk_rising_edge;
    always_ff @ (posedge Clk) begin
        frame_clk_delayed <= frame_clk;
        frame_clk_rising_edge <= (frame_clk == 1'b1) && (frame_clk_delayed == 1'b0);
		  if ((frame_clk == 1'b1) && (frame_clk_delayed == 1'b0) && ~won) 
		  begin
		      counter <= counter + 8'd1;
		      if (counter >= 8'd03)
				begin
		  		for (int i = 47; i > 0; i--)
					begin
						rightChannelSamples[i] <= rightChannelSamples[i-1];
						leftChannelSamples[i] <= leftChannelSamples[i-1];
					end
					counter <= 8'd0;
				end
				
					rightChannelSamples[0] <= ADCDATA[15:0];
					leftChannelSamples[0] <= ADCDATA[31:16];
					
		  end
					
    end
    // Update registers
    always_ff @ (posedge Clk)
    begin
        if (Reset || is_dead)
        begin
            Ball_X_Pos <= Ball_X_Center;
            Ball_Y_Pos <= Ball_Y_Center;
            Ball_X_Motion <= 10'd0;
            Ball_Y_Motion <= 10'd0;
				score <= 19'd0;
        end
        else if(~won)
        begin
            Ball_X_Pos <= Ball_X_Pos_in;
            Ball_Y_Pos <= Ball_Y_Pos_in;
            Ball_X_Motion <= Ball_X_Motion_in;
            Ball_Y_Motion <= Ball_Y_Motion_in;
				score <= nextScore;
        end
    end
    always_comb
    begin
        // By default, keep motion and position unchanged
        Ball_X_Pos_in = Ball_X_Pos;
        Ball_Y_Pos_in = Ball_Y_Pos;
        Ball_X_Motion_in = Ball_X_Motion;
        Ball_Y_Motion_in = Ball_Y_Motion;
		  
		  nextScore = score;
        
        // Update position and motion only at rising edge of frame clock
        if (frame_clk_rising_edge)
        begin
		  
				nextScore = score + (Ball_X_Pos - Ball_X_Center) * (Ball_X_Pos - Ball_X_Center)/16'd30;
		  
            if( Ball_Y_Pos + Ball_Size >= Ball_Y_Max )  // Ball is at the bottom edge, BOUNCE!
					begin
						Ball_X_Motion_in = 0;
						Ball_Y_Motion_in = (~(Ball_Y_Step) + 1'b1);  // 2's complement.  
					end
            else if ( Ball_Y_Pos <= Ball_Y_Min + Ball_Size )  // Ball is at the top edge, BOUNCE!
					begin
						Ball_X_Motion_in = 0;
						Ball_Y_Motion_in = Ball_Y_Step;
					end
				
				else if( Ball_X_Pos + Ball_Size >= Ball_X_Max )  // Ball is at the bottom edge, BOUNCE!
					begin
						Ball_X_Motion_in = (~(Ball_X_Step) + 1'b1);  // 2's complement.  
						Ball_Y_Motion_in = 0;
					end
            else if ( Ball_X_Pos <= Ball_X_Min + Ball_Size )  // Ball is at the top edge, BOUNCE!
					begin
						Ball_X_Motion_in = Ball_X_Step;
						Ball_Y_Motion_in = 0;
					end
				else
				begin
					if (move_left)
						begin
							Ball_X_Motion_in = (~(Ball_Y_Step) + 1'b1);  // 2's complement. 
							Ball_Y_Motion_in = 0;
						end
					else if (move_right)
						begin
							Ball_X_Motion_in = Ball_Y_Step;
							Ball_Y_Motion_in = 0;
						end
					else if (stand_still)
						begin
							Ball_X_Motion_in = 0;
							Ball_Y_Motion_in = 0;
						end
					else
					begin
					case(keycode)
						8'h1A: //W
						begin
							Ball_X_Motion_in = 0;
							Ball_Y_Motion_in = 0;
						end
						8'h16: //S
						begin
							Ball_X_Motion_in = 0;
							Ball_Y_Motion_in = 0;
						end
						8'h04: //A
						begin
							Ball_X_Motion_in = (~(Ball_Y_Step) + 1'b1);  // 2's complement. 
							Ball_Y_Motion_in = 0;
						end
						8'h07: //D
						begin
							Ball_X_Motion_in = Ball_Y_Step;
							Ball_Y_Motion_in = 0;
						end
						
						default : ;        
					endcase
					end
				end
				
            // Update the ball's position with its motion
            Ball_X_Pos_in = Ball_X_Pos + Ball_X_Motion;
            Ball_Y_Pos_in = Ball_Y_Pos + Ball_Y_Motion;
        end
    end
    
    // Compute whether the pixel corresponds to ball or background
    /* Since the multiplicants are required to be signed, we have to first cast them
       from logic to int (signed by default) before they are multiplied. */
    int DistX, DistY, Size;
    assign DistX = DrawX - Ball_X_Pos;
    assign DistY = DrawY - Ball_Y_Pos;
    assign Size = Ball_Size;
	
    always_comb begin
		  if (DrawX > 16'd320 - 16'd2*Ball_Size && DrawX < 16'd320 + 16'd2*Ball_Size) 
		  begin
		      is_wall = 1'b0;
		  end
		  else if (DrawX < 16'd3* leftChannelSamples[DrawY/16'd10]/16'd205 || DrawX > (16'd639 - 16'd3*rightChannelSamples[DrawY/16'd10]/16'd205)) 
            is_wall = 1'b1;
		  else 
				is_wall = 1'b0;
		  if ( ( DistX*DistX + DistY*DistY) <= (Size*Size) ) 
            is_ball = 1'b1;
        else
            is_ball = 1'b0;
        /* The ball's (pixelated) circle is generated using the standard circle formula.  Note that while 
           the single line is quite powerful descriptively, it causes the synthesis tool to use up three
           of the 12 available multipliers on the chip! */
    end
    
endmodule

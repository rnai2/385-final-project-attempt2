module reverbSRAM
(
	input logic Clk, Reset_ah, writeZero,
	input logic [15:0] LData_In,
	input logic [15:0] RData_In,
	input logic [15:0] multGain,
	input logic [15:0] divGain,
	output logic [15:0] LData_Out,
	output logic [15:0] RData_Out,
	output logic [19:0] SRAM_ADDR,
	inout wire   [15:0] SRAM_DQ,
	output logic        SRAM_UB_N,
	output logic        SRAM_LB_N,
	output logic        SRAM_CE_N,
	output logic        SRAM_OE_N,
	output logic        SRAM_WE_N,
	output logic [17:0] loopCount
);

enum logic [3:0]
{
	READRIGHT0,
	READRIGHT1,
	WRITERIGHT0,
	WRITERIGHT1,
	READLEFT0,
	READLEFT1,
	WRITELEFT0,
	WRITELEFT1
}   State, Next_state;   // Internal state logic

assign SRAM_CE_N = 1'b0;
assign SRAM_UB_N = 1'b0;
assign SRAM_LB_N = 1'b0;

logic [15:0] Data_to_SRAM;
logic [15:0] Data_from_SRAM;

logic [15:0] EchoLeftSample;
logic [15:0] EchoRightSample;

logic [15:0] LData_toSRAM;
logic [15:0] RData_toSRAM;


tristate #(.N(16)) tr0(.Clk(Clk), .tristate_output_enable(~SRAM_WE_N), .Data_write(Data_to_SRAM), .Data_read(Data_from_SRAM), .Data(SRAM_DQ));

always_comb
begin
	SRAM_WE_N = 1'b1;
	SRAM_OE_N = 1'b1;
	Next_state = State;
	Data_to_SRAM = 16'hXXXX;
	
	if(divGain > multGain)
	begin
		LData_Out = (LData_In) + (EchoLeftSample*multGain/divGain);
		RData_Out = (RData_In) + (EchoRightSample*multGain/divGain);
	end
	else
	begin
		LData_Out = (LData_In) + (EchoLeftSample*4/5);
		RData_Out = (RData_In) + (EchoRightSample*4/5);
	end
	
	case(State)
	READRIGHT0:
	begin
		Next_state = READRIGHT1;
		SRAM_OE_N = 1'b0;
	end
	
	READRIGHT1:
	begin
		Next_state = WRITERIGHT0;
		SRAM_OE_N = 1'b0;
	end
	
	WRITERIGHT0:
	begin
		Next_state = WRITERIGHT1;
		SRAM_WE_N = 1'b0;
		if(writeZero)
			Data_to_SRAM = 0;
		else
			Data_to_SRAM = RData_toSRAM;
	end
	
	WRITERIGHT1:
	begin
		Next_state = READLEFT0;
		SRAM_WE_N = 1'b0;
		if(writeZero)
			Data_to_SRAM = 0;
		else
			Data_to_SRAM = RData_toSRAM;
	end
	
	READLEFT0:
	begin
		Next_state = READLEFT1;
		SRAM_OE_N = 1'b0;
	end
	
	READLEFT1:
	begin
		Next_state = WRITELEFT0;
		SRAM_OE_N = 1'b0;
	end
	
	WRITELEFT0:
	begin
		Next_state = WRITELEFT1;
		SRAM_WE_N = 1'b0;
		if(writeZero)
			Data_to_SRAM = 0;
		else
			Data_to_SRAM = LData_toSRAM;
	end
	
	WRITELEFT1:
	begin
		Next_state = READRIGHT0;
		SRAM_WE_N = 1'b0;
		if(writeZero)
			Data_to_SRAM = 0;
		else
			Data_to_SRAM = LData_toSRAM;
	end

	default : ;
	
	endcase
end

always_ff @ (posedge Clk)
begin
	if(Reset_ah)
	begin
		SRAM_ADDR <= 0;
		State <= READRIGHT0;
		loopCount <= 0;
	end
	else
	begin
		State <= Next_state;
		
		if(SRAM_ADDR == 20'd0)
		begin
			loopCount <= loopCount + 18'd1;
	end
		
	case(State)
	
		READRIGHT0:
			RData_toSRAM <= RData_Out;
	
		READRIGHT1:
			EchoRightSample <= Data_from_SRAM;
		
		WRITERIGHT1:
			SRAM_ADDR <= SRAM_ADDR + 20'd1;
		
		READLEFT0:
			LData_toSRAM <= LData_Out;
		
		READLEFT1:
			EchoLeftSample <= Data_from_SRAM;
		
		WRITELEFT1:
			SRAM_ADDR <= SRAM_ADDR + 20'd1;
			
		default : ;
		
	endcase
	end
end

endmodule
module equalizer
(
	input      CLOCK_50,
	input        [3:0]  KEY,          //bit 0 is set up as Reset
	input	     [17:0] SW,
	input               AUD_ADCDAT, AUD_DACLRCK, AUD_ADCLRCK, AUD_BCLK,
	output logic        AUD_DACDAT, AUD_XCK, I2C_SCLK, I2C_SDAT,
	output logic [6:0]  HEX0, HEX1, HEX2, HEX3, HEX4, HEX5, HEX6, HEX7,
	output logic [7:0]  LEDG,
	output logic [17:0] LEDR,
	// VGA Interface 
	output logic [7:0]  VGA_R,        //VGA Red
							  VGA_G,        //VGA Green
							  VGA_B,        //VGA Blue
	output logic        VGA_CLK,      //VGA Clock
							  VGA_SYNC_N,   //VGA Sync signal
							  VGA_BLANK_N,  //VGA Blank signal
							  VGA_VS,       //VGA virtical sync signal
							  VGA_HS,       //VGA horizontal sync signal
	// CY7C67200 Interface
	inout  wire  [15:0] OTG_DATA,     //CY7C67200 Data bus 16 Bits
	output logic [1:0]  OTG_ADDR,     //CY7C67200 Address 2 Bits
	output logic        OTG_CS_N,     //CY7C67200 Chip Select
							  OTG_OE_N,     //CY7C67200 Write RD
							  OTG_WE_N,     //CY7C67200 Read WR
							  OTG_RST_N,    //CY7C67200 Reset
	input        [1:0]  OTG_INT,      //CY7C67200 Interrupt
	// SDRAM Interface for Nios II Software
	output logic [12:0] DRAM_ADDR,    //SDRAM Address 13 Bits
	inout  wire  [31:0] DRAM_DQ,      //SDRAM Data 32 Bits
	output logic [1:0]  DRAM_BA,      //SDRAM Bank Address 2 Bits
	output logic [3:0]  DRAM_DQM,     //SDRAM Data Mast 4 Bits
	output logic        DRAM_RAS_N,   //SDRAM Row Address Strobe
							  DRAM_CAS_N,   //SDRAM Column Address Strobe
							  DRAM_CKE,     //SDRAM Clock Enable
							  DRAM_WE_N,    //SDRAM Write Enable
							  DRAM_CS_N,    //SDRAM Chip Select
							  DRAM_CLK,      //SDRAM Clock
	// SRAM Interface
	output logic [19:0] SRAM_ADDR,
	inout wire   [15:0] SRAM_DQ,
	output logic        SRAM_UB_N,
	output logic        SRAM_LB_N,
	output logic        SRAM_CE_N,
	output logic        SRAM_OE_N,
	output logic        SRAM_WE_N
);

HexDriver hex_inst_0 (divGain[15:12], HEX7);
HexDriver hex_inst_1 (divGain[11:8], HEX6);
HexDriver hex_inst_2 (divGain[7:4], HEX5);
HexDriver hex_inst_3 (divGain[3:0], HEX4);

HexDriver hex_inst_4 (multGain[15:12], HEX3);
HexDriver hex_inst_5 (multGain[11:8], HEX2);
HexDriver hex_inst_6 (multGain[7:4], HEX1);
HexDriver hex_inst_7 (multGain[3:0], HEX0);

logic [15:0] reverbL;
logic [15:0] reverbR;

logic [15:0] LDATA, RDATA, LDATA_IN, RDATA_IN;
logic [31:0] ADCDATA; //data from line in 
logic INIT, INIT_FINISH, adc_full, data_over;

audio_interface ai
(
	.LDATA(LDATA), .RDATA(RDATA), .clk(CLOCK_50), .Reset(~KEY[0]), .INIT(INIT),
	.INIT_FINISH(INIT_FINISH), .adc_full(adc_full), .data_over(data_over),
	.AUD_MCLK(AUD_XCK), .AUD_BCLK(AUD_BCLK), .AUD_ADCDAT(AUD_ADCDAT),
	.AUD_DACDAT(AUD_DACDAT), .AUD_DACLRCK(AUD_DACLRCK), .AUD_ADCLRCK(AUD_ADCLRCK),
	.I2C_SDAT(I2C_SDAT), .I2C_SCLK(I2C_SCLK), .ADCDATA(ADCDATA)
);

logic [15:0] divGain;
logic [15:0] multGain;

logic [7:0] keycode;

logic [1:0] hpi_addr;
logic [15:0] hpi_data_in, hpi_data_out;
logic hpi_r, hpi_w, hpi_cs, hpi_reset;
    
// Interface between NIOS II and EZ-OTG chip
hpi_io_intf hpi_io_inst(
								 .Clk(CLOCK_50),
								 .Reset(~KEY[0]),
								 // signals connected to NIOS II
								 .from_sw_address(hpi_addr),
								 .from_sw_data_in(hpi_data_in),
								 .from_sw_data_out(hpi_data_out),
								 .from_sw_r(hpi_r),
								 .from_sw_w(hpi_w),
								 .from_sw_cs(hpi_cs),
								 .from_sw_reset(hpi_reset),
								 // signals connected to EZ-OTG chip
								 .OTG_DATA(OTG_DATA),    
								 .OTG_ADDR(OTG_ADDR),    
								 .OTG_RD_N(OTG_OE_N),    
								 .OTG_WR_N(OTG_WE_N),    
								 .OTG_CS_N(OTG_CS_N),
								 .OTG_RST_N(OTG_RST_N)
);

equalizer_soc soc_inst (.clk_clk(CLOCK_50),
											 .div_gain_wire_export(divGain),
											 .keycode_export(keycode),
											 .mult_gain_wire_export(multGain),
											 .otg_hpi_address_export(hpi_addr),
											 .otg_hpi_cs_export(hpi_cs),
											 .otg_hpi_data_in_port(hpi_data_in),
											 .otg_hpi_data_out_port(hpi_data_out),
											 .otg_hpi_r_export(hpi_r),
											 .otg_hpi_reset_export(hpi_reset),
											 .otg_hpi_w_export(hpi_w),
											 .reset_reset_n(1'b1), 
											 .sdram_wire_addr(DRAM_ADDR),    //  sdram_wire.addr
											 .sdram_wire_ba(DRAM_BA),      	//  .ba
											 .sdram_wire_cas_n(DRAM_CAS_N),    //  .cas_n
											 .sdram_wire_cke(DRAM_CKE),     	//  .cke
											 .sdram_wire_cs_n(DRAM_CS_N),      //  .cs_n
											 .sdram_wire_dq(DRAM_DQ),      	//  .dq
											 .sdram_wire_dqm(DRAM_DQM),     	//  .dqm
											 .sdram_wire_ras_n(DRAM_RAS_N),    //  .ras_n
											 .sdram_wire_we_n(DRAM_WE_N),      //  .we_n
											 .sdram_clk_clk(DRAM_CLK)			//  clock out to SDRAM from other PLL port
											 );

logic [17:0] loopCount;

reverbSRAM rev(.Clk(CLOCK_50), .Reset_ah(~KEY[0]), .writeZero(SW[1]), .LData_In(LDATA_IN), .RData_In(RDATA_IN), .LData_Out(reverbL), .RData_Out(reverbR), .*);

enum logic [1:0]
{
	START,
	OPERATE
}   State, Next_state;   // Internal state logic

always_comb
	begin 
	
//		keycode = 8'd0;
//		if(~KEY[2])
//		begin
//		keycode = 8'h04;
//		end
//		else if(~KEY[1])
//		begin
//		keycode = 8'h07;
//		end
		LEDG[0] = 1'b1;
		LEDG[1] = INIT_FINISH;
		LEDG[2] = 1'b0;
		LEDG[3] = adc_full;
		LEDG[4] = SW[0] & ~SW[1];
		LEDG[5] = SW[0];
		
		// Default next state is staying at current state
		Next_state = State;
		INIT = 1'b0;
		unique case (State)
			START:
			begin
				INIT = 1'b1;
				if (INIT_FINISH)
					Next_state = OPERATE;
			end
			OPERATE:
				LEDG[2] = 1'b1;
			default: ;
		endcase
	end
	
always_ff @ (posedge CLOCK_50)
	begin
		if(~KEY[0])
			State <= START;
		else
		begin
			State <= Next_state;
			if(State == OPERATE)
			begin
				if(adc_full)
				begin
					LDATA_IN <= ADCDATA[31:16] - 16'hF000;//ReverbLOut;
					RDATA_IN <= ADCDATA[15:0] - 16'hF000;//ReverbROut;
				end
				if(data_over)
				begin
					if(SW[0])
					begin
						LDATA <= reverbL;
						RDATA <= reverbR;
					end
					else
					begin
						LDATA <= LDATA_IN;
						RDATA <= RDATA_IN;
					end
				end
			end
		end
	end
    vga_clk vga_clk_instance(.inclk0(CLOCK_50), .c0(VGA_CLK));
    
    // TODO: Fill in the connections for the rest of the modules 
	 
	 logic [9:0] DrawX, DrawY;
	 
    VGA_controller vga_controller_instance(.Reset(~KEY[0]), .Clk(CLOCK_50), .*);
    
    // Which signal should be frame_clk?
	 logic is_ball;
	 logic is_wall;
	 logic is_dead;
	 logic won;
	 logic [18:0] score;
    ball ball_instance(.frame_clk(VGA_VS), .Reset(~KEY[0]), .Clk(CLOCK_50), .ADCDATA({LDATA, RDATA}), .move_left(~KEY[3]), .move_right(~KEY[1]), .stand_still(~KEY[2]), .*);
	 color_mapper color_instance(.reverb_on(SW[0] & ~SW[1]), .*);
	 assign LEDR = score[17:0];
	 
    
endmodule

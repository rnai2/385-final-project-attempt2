//-------------------------------------------------------------------------
//    Color_Mapper.sv                                                    --
//    Stephen Kempf                                                      --
//    3-1-06                                                             --
//                                                                       --
//    Modified by David Kesler  07-16-2008                               --
//    Translated by Joe Meng    07-07-2013                               --
//    Modified by Po-Han Huang  10-06-2017                               --
//                                                                       --
//    Fall 2017 Distribution                                             --
//                                                                       --
//    For use with ECE 385 Lab 8                                         --
//    University of Illinois ECE Department                              --
//-------------------------------------------------------------------------


`define CHAR1Y 10

`define REV1X 250
`define REV2X (`REV1X + 8)
`define REV3X (`REV2X + 8)
`define REV4X (`REV3X + 8)
`define REV5X (`REV4X + 8)
`define REV6X (`REV5X + 8)



`define CHAR1X 298
`define CHAR2X (`CHAR1X + 8)
`define CHAR3X (`CHAR2X + 8)
`define CHAR4X (`CHAR3X + 8)
`define CHAR5X (`CHAR4X + 8)
`define CHAR6X (`CHAR5X + 8)
`define CHAR7X (`CHAR6X + 8)
`define CHAR8X (`CHAR7X + 8)
`define CHAR9X (`CHAR8X + 8)
`define CHAR10X (`CHAR9X + 8)

`define WON1Y 160
`define WON1X 280
`define WON2X (`WON1X + 8)
`define WON3X (`WON2X + 8)
`define WON4X (`WON3X + 8)
`define WON5X (`WON4X + 8)
`define WON6X (`WON5X + 8)
`define WON7X (`WON6X + 8)
`define WON8X (`WON7X + 8) 
`define SIZEX 8
`define SIZEY 16 

// color_mapper: Decide which color to be output to VGA for each pixel.
module  color_mapper ( input              is_ball,            // Whether current pixel belongs to ball 
                       input					is_wall,            //   or background (computed in ball.sv)
							  input              won, reverb_on,
                       input        [9:0] DrawX, DrawY,       // Current pixel coordinates
                       output logic [7:0] VGA_R, VGA_G, VGA_B, // VGA RGB output
							  output logic is_dead
                     );
    
    logic [7:0] Red, Green, Blue;
    // wall color should be the inverse of the background color, where the background color is some function of positon
    // Output colors to VGA
    assign VGA_R = Red;
    assign VGA_G = Green;
    assign VGA_B = Blue;
    // Assign color based on is_ball signal
    always_comb
    begin
		  is_dead = 1'b0;
	     if (is_ball == 1'b1 && is_wall == 1'b1)
		  begin
			   is_dead = 1'b1;	
		  end
        if (is_ball == 1'b1) 
        begin
            // White ball
            Red = DrawY[8:1];
            Green = DrawX[9:2] + DrawY[9:2];
            Blue = DrawX[8:1];
        end
		  
		  else if  ((shape_rev == 1'b1) && reverb_on && ((sprite_data[8 - (DrawX - `REV1X)] == 1'b1) ||
													  (sprite_data[8 - (DrawX - `REV2X)] == 1'b1) ||
													  (sprite_data[8 - (DrawX - `REV3X)] == 1'b1) ||
													  (sprite_data[8 - (DrawX - `REV4X)] == 1'b1) ||
													  (sprite_data[8 - (DrawX - `REV5X)] == 1'b1) ||
													  (sprite_data[8 - (DrawX - `REV6X)] == 1'b1)))
		  begin
			Red = 8'hff;
			Green = 8'h00;
			Blue = 8'h00;
		  end
		  
		  else if  ((shape_on == 1'b1) && ((sprite_data[8 - (DrawX - `CHAR1X)] == 1'b1) ||
													  (sprite_data[8 - (DrawX - `CHAR2X)] == 1'b1) ||
													  (sprite_data[8 - (DrawX - `CHAR3X)] == 1'b1) ||
													  (sprite_data[8 - (DrawX - `CHAR4X)] == 1'b1) ||
													  (sprite_data[8 - (DrawX - `CHAR5X)] == 1'b1) ||
													  (sprite_data[8 - (DrawX - `CHAR6X)] == 1'b1) ||
													  (sprite_data[8 - (DrawX - `CHAR7X)] == 1'b1) ||
													  (sprite_data[8 - (DrawX - `CHAR8X)] == 1'b1) ||
													  (sprite_data[8 - (DrawX - `CHAR9X)] == 1'b1) ||
													  (sprite_data[8 - (DrawX - `CHAR10X)] == 1'b1)))
								
		  begin
			Red = 8'hff;
			Green = 8'h00;
			Blue = 8'h00;
		  end
		  else if  ((shape_won == 1'b1) && won && ((sprite_data[8 - (DrawX - `WON1X)] == 1'b1) ||
													  (sprite_data[8 - (DrawX - `WON2X)] == 1'b1) ||
													  (sprite_data[8 - (DrawX - `WON3X)] == 1'b1) ||
													  (sprite_data[8 - (DrawX - `WON5X)] == 1'b1) ||
													  (sprite_data[8 - (DrawX - `WON6X)] == 1'b1) ||
													  (sprite_data[8 - (DrawX - `WON7X)] == 1'b1) ||
													  (sprite_data[8 - (DrawX - `WON8X)] == 1'b1)))
								
		  begin
			Red = 8'h00;
			Green = 8'hff;
			Blue = 8'hff;
		  end
		  
		  else if (is_wall == 1'b1) 
		  begin
				Red = DrawX[9:2] - 8'd30; 
            Green = DrawY[9:2] - 8'd30;
            Blue = DrawX[7:0] + DrawY[7:0] - 8'd30;
		  end
		  
        else 
        begin
            // Background with nice color gradient
            Red = DrawX[9:2]; 
            Green = DrawY[9:2];
            Blue = DrawX[7:0] + DrawY[7:0];
        end
    end 
	 
	 logic shape_on;
	 logic shape_won;
	 logic shape_rev;
	 logic [10:0] sprite_addr;
	 logic [7:0] sprite_data;
	 font_rom(.addr(sprite_addr), .data(sprite_data));
	 
	 always_comb
	 begin:Ball_on_proc
		shape_on = 1'b0;
		shape_won = 1'b0;
		shape_rev = 1'b0;
		sprite_addr = 10'b0;
		if(DrawX >= `REV1X && DrawX < `REV1X + `SIZEX && DrawY >= `CHAR1Y && DrawY < `CHAR1Y + `SIZEY)
			begin
			shape_rev = 1'b1;
			sprite_addr = (DrawY-`CHAR1Y + 16*'h72); //r
			end
		else if(DrawX >= `REV2X && DrawX < `REV2X + `SIZEX && DrawY >= `CHAR1Y && DrawY < `CHAR1Y + `SIZEY)
			begin
			shape_rev = 1'b1;
			sprite_addr = (DrawY-`CHAR1Y + 16*'h65);//e
			end
		else if(DrawX >= `REV3X && DrawX < `REV3X + `SIZEX && DrawY >= `CHAR1Y && DrawY < `CHAR1Y + `SIZEY)
			begin
			shape_rev = 1'b1;
			sprite_addr = (DrawY-`CHAR1Y + 16*'h76);//v
			end
		else if(DrawX >= `REV4X && DrawX < `REV4X + `SIZEX && DrawY >= `CHAR1Y && DrawY < `CHAR1Y + `SIZEY)
			begin
			shape_rev = 1'b1;
			sprite_addr = (DrawY-`CHAR1Y + 16*'h65);//e
			end
		else if(DrawX >= `REV5X && DrawX < `REV5X + `SIZEX && DrawY >= `CHAR1Y && DrawY < `CHAR1Y + `SIZEY)
			begin
			shape_rev = 1'b1;
			sprite_addr = (DrawY-`CHAR1Y + 16*'h72);//r
			end
		else if(DrawX >= `REV6X && DrawX < `REV6X + `SIZEX && DrawY >= `CHAR1Y && DrawY < `CHAR1Y + `SIZEY)
			begin
			shape_rev = 1'b1;
			sprite_addr = (DrawY-`CHAR1Y + 16*'h62);//b
			end
			
			
		if(DrawX >= `CHAR1X && DrawX < `CHAR1X + `SIZEX && DrawY >= `CHAR1Y && DrawY < `CHAR1Y + `SIZEY)
			begin
			shape_on = 1'b1;
			sprite_addr = (DrawY-`CHAR1Y + 16*'h4d); //M
			end
		else if(DrawX >= `CHAR2X && DrawX < `CHAR2X + `SIZEX && DrawY >= `CHAR1Y && DrawY < `CHAR1Y + `SIZEY)
			begin
			shape_on = 1'b1;
			sprite_addr = (DrawY-`CHAR1Y + 16*'h55); //U
			end
		else if(DrawX >= `CHAR3X && DrawX < `CHAR3X + `SIZEX && DrawY >= `CHAR1Y && DrawY < `CHAR1Y + `SIZEY)
			begin
			shape_on = 1'b1;
			sprite_addr = (DrawY-`CHAR1Y + 16*'h54); //T
			end
		else if(DrawX >= `CHAR4X && DrawX < `CHAR4X + `SIZEX && DrawY >= `CHAR1Y && DrawY < `CHAR1Y + `SIZEY)
			begin
			shape_on = 1'b1;
			sprite_addr = (DrawY-`CHAR1Y + 16*'h49); //I
			end
		else if(DrawX >= `CHAR5X && DrawX < `CHAR5X + `SIZEX && DrawY >= `CHAR1Y && DrawY < `CHAR1Y + `SIZEY)
			begin
			shape_on = 1'b1;
			sprite_addr = (DrawY-`CHAR1Y + 16*'h4c); //L
			end
		else if(DrawX >= `CHAR6X && DrawX < `CHAR6X + `SIZEX && DrawY >= `CHAR1Y && DrawY < `CHAR1Y + `SIZEY)
			begin
			shape_on = 1'b1;
			sprite_addr = (DrawY-`CHAR1Y + 16*'h41); //A
			end
		else if(DrawX >= `CHAR7X && DrawX < `CHAR7X + `SIZEX && DrawY >= `CHAR1Y && DrawY < `CHAR1Y + `SIZEY)
			begin
			shape_on = 1'b1;
			sprite_addr = (DrawY-`CHAR1Y + 16*'h54); //T
			end
		else if(DrawX >= `CHAR8X && DrawX < `CHAR8X + `SIZEX && DrawY >= `CHAR1Y && DrawY < `CHAR1Y + `SIZEY)
			begin
			shape_on = 1'b1;
			sprite_addr = (DrawY-`CHAR1Y + 16*'h49); //I
			end
		else if(DrawX >= `CHAR9X && DrawX < `CHAR9X + `SIZEX && DrawY >= `CHAR1Y && DrawY < `CHAR1Y + `SIZEY)
			begin
			shape_on = 1'b1;
			sprite_addr = (DrawY-`CHAR1Y + 16*'h4f); //O
			end
		else if(DrawX >= `CHAR10X && DrawX < `CHAR10X + `SIZEX && DrawY >= `CHAR1Y && DrawY < `CHAR1Y + `SIZEY)
			begin
			shape_on = 1'b1;
			sprite_addr = (DrawY-`CHAR1Y + 16*'h4e); //N
			end
		//else if (DrawX >= shape_x && DrawX < shape_x + shape_size_x && DrawY >= shape_y + shape_size_y && DrawY < shape_y + shape_size_y) {
		
		//}
		if(DrawX >= `WON1X && DrawX < `WON1X + `SIZEX && DrawY >= `WON1Y && DrawY < `WON1Y + `SIZEY)
			begin
			shape_won = 1'b1;
			sprite_addr = (DrawY-`WON1Y + 16*'h59); //Y
			end
		else if(DrawX >= `WON2X && DrawX < `WON2X + `SIZEX && DrawY >= `WON1Y && DrawY < `WON1Y + `SIZEY)
			begin
			shape_won = 1'b1;
			sprite_addr = (DrawY-`WON1Y + 16*'h4f);//O
			end
		else if(DrawX >= `WON3X && DrawX < `WON3X + `SIZEX && DrawY >= `WON1Y && DrawY < `WON1Y + `SIZEY)
			begin
			shape_won = 1'b1;
			sprite_addr = (DrawY-`WON1Y + 16*'h55);//U
			end

		else if(DrawX >= `WON5X && DrawX < `WON5X + `SIZEX && DrawY >= `WON1Y && DrawY < `WON1Y + `SIZEY)
			begin
			shape_won = 1'b1;
			sprite_addr = (DrawY-`WON1Y + 16*'h57);//W
			end
		else if(DrawX >= `WON6X && DrawX < `WON6X + `SIZEX && DrawY >= `WON1Y && DrawY < `WON1Y + `SIZEY)
			begin
			shape_won = 1'b1;
			sprite_addr = (DrawY-`WON1Y + 16*'h49);//I
			end
		else if(DrawX >= `WON7X && DrawX < `WON7X + `SIZEX && DrawY >= `WON1Y && DrawY < `WON1Y + `SIZEY)
			begin
			shape_won = 1'b1;
			sprite_addr = (DrawY-`WON1Y + 16*'h4e); //N
			end
		else if(DrawX >= `WON8X && DrawX < `WON8X + `SIZEX && DrawY >= `WON1Y && DrawY < `WON1Y + `SIZEY)
			begin
			shape_won = 1'b1;
			sprite_addr = (DrawY-`WON1Y + 16*'h21); //!
			end
	end
	 
	 //always_ff @ (posedge Clk)
    
endmodule
